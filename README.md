# Repositorio plantilla: "Cantidades"

Para entregar este ejercicio, crea una bifurcación (fork) de este repositorio, y sube a él tu solución. Puedes consultar el [enunciado](https://gitlab.etsit.urjc.es/cursoprogram/materiales/-/blob/main/practicas/ejercicios/README.md#cantidades), que incluye la fecha de entrega.
